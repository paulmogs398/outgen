import numpy as np
import pandas as pd
from sklearn import datasets # Replace with python random


def do_checks(df):
    pass

def find_center(df):
    """takes a cluster of points, calcuates the centers for each dimmension returns a array of the means."""

    # Makes sure the dataframe has instances
    if len(df) == 0:
        raise Exception('Empty cluster found')

    # The array to contain the center
    center = []

    # Sum up values for each feature
    for f in range(len(df.columns[:-1])):        
        # Work out average
        average = df.iloc[:, f].sum() / len(df)            
        # collect centers for each dimmensions
        center.append(average)
    
    return center

def create_blob(center, number_of_features, random_state, std_dev=0.7):
    """create a dataframe containing a blob of points at given point."""

    # Check center position is between 0 and 1
    for c in center:
        if c > 1 or c < 0: 
            raise Exception('Data is not normalised between 0 and 1')
        
    # Check standard deviation is not unreasonably large
    if std_dev > 4 or std_dev < 0.1:
        raise Exception('Standard deviation is too large/small')

    # Generate blobs
    blobs = datasets.make_blobs(n_samples = 500, centers = [center],
                            n_features = number_of_features, cluster_std = std_dev, random_state=random_state)
    
    return blobs[0]