from .outils import *

import numpy as np
import pandas as pd

def is_outlier(instance, clusters):
    #  Ranges nested list
    #  [low / high]  [cluster]  [dimmension]
    for cluster in range(len(clusters[0])):
        # The values must be outside the box, outside in any dimmension
        # 0 or 1 it does not matter number of ranges should consistent
        inside = True
        for dimension in range(len(clusters[0][cluster])):
            # Is it inlier?
            if (instance[dimension] > clusters[0][cluster][dimension] and
                    instance[dimension] < clusters[1][cluster][dimension]):
                # Yes it is inside
                continue
            else:
                inside = False
                break
        if inside:
            # This instance is inside a box
            return False
    
    # This instance is not in any of the ranges
    return True

    # Regarding low/high q1 and q3 would be 25/75 but could be 10/90
def find_quartiles_for_clusters(df, high, low):
    low_quartiles = []
    high_quartiles = []
    
    # Per clsuter
    for label in df[df.columns[-1]].unique():
        
        df_temp = df.loc[ df[df.columns[-1]] == label]
        
        #drop label column
        df_temp = df_temp.drop(df_temp.columns[-1], axis=1)
        
        low_quartiles = low_quartiles + [np.percentile(df_temp, low, axis=0).tolist()]  # Q1
        high_quartiles = high_quartiles + [np.percentile(df_temp, high, axis=0).tolist()]  # Q3
        
    #Returns an array containing each q & q3 > cluster > dimmension
    return [low_quartiles, high_quartiles]

def add_iqr_outliers(df, label, n_outliers, outlier_source, cluster_quartiles):
    """Adds true outliers to a dataset"""
    
    # Check that number to add is sensible
    if n_outliers > 20:
        raise Exception("To avoid generating inliers, add fewer outliers")

    # Check there is an adquate pool of outliers to pull from
    if len(outlier_source) < 100:
        raise Exception("In need to a big pool to potentially make outliers")
    
    # Check df columns matches the number of outliers columns bar the class column
    if (len(df.columns)-1) != len(outlier_source[0]):
        raise Exception("Mis-match number of columns between outlier and datset")
    
    # Loop over the potential cadidates to add
    for outlier in outlier_source:
        
        # Do we need to add more outliers
        if n_outliers <= 0:
            return df
        
        if is_outlier(outlier, cluster_quartiles):
            # Yes it is, add it to the dataset
            df_outlier = pd.DataFrame([outlier], columns=df.columns[:-1])
            df_outlier['class'] = label
            df = df.append(df_outlier, ignore_index=True)            
            n_outliers = n_outliers - 1
    
    # Check it managed to successfully add all the required outliers
    if n_outliers > 0:
        raise Exception("Failed to add require quantity of outliers, consider adjusting the lof_threshold, providing a bigger outlier sample, making sure the dataset is normalised between 1 and 0")
    
    return df

def add_outliers(df, n_outliers, random_state=1, high=75, low=25):
    # Calculate quartiles for the clusters
    cluster_quartiles = find_quartiles_for_clusters(df, high, low)

    # Loop over each label
    for label in df[df.columns[-1]].unique():
        # caluate the center (mean) of data, to center the blobs at
        center = find_center(df[df[df.columns[-1]] == label])
        # create a guassian blob at the center piont, to draw outliers from
        blob = create_blob(center, len(df.columns)-1, random_state)
        # Add true outliers
        df = add_iqr_outliers(df, label, n_outliers, blob, cluster_quartiles)            
    return df