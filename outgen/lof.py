from .outils import *

import numpy as np
import pandas as pd
from sklearn import neighbors

def add_lof_outliers(df, label, n_outliers, lof_neighbors, lof_threshold, outlier_source):

    # Check that number to add is sensible
    if n_outliers > 20:
        raise Exception("To avoid generating inliers, add fewer outliers")
    
    # Check there is an adquate pool of outliers to pull from
    if len(outlier_source) < 100:
        raise Exception("In need to a big pool to potentially make outliers")
    
    # Check df columns matches the number of outliers columns bar the class column
    if (len(df.columns)-1) != len(outlier_source[0]):
        raise Exception("Mis-match number of columns between outlier and datset")
    
    # Loop over the potential cadidates to add
    for outlier in outlier_source:
        
        # Do we need to add more outliers
        if n_outliers <= 0:
            return df
        
        # Convert individual outlier to dataset
        df_outlier = pd.DataFrame(np.array([outlier]), columns=df.columns[:-1])

        # Append outlier to a copy of the dataset with on class column
        df_data_plus_outlier = df[df.columns[:-1]].append(df_outlier, ignore_index=True)
        
        # Calculate the LOF for instance weights over the whole dataset pre-clustering
        clf = neighbors.LocalOutlierFactor(n_neighbors=lof_neighbors)
        clf.fit_predict(df_data_plus_outlier)
    
        # Scores for how outlying each instance is
        # LOF -1 for anomalies/outliers and 1 for inliers. 
        x_scores = clf.negative_outlier_factor_
    
        #print(x_scores[-1])
    
        # Is the outlier we added indeed an outlier?
        # which should be the last one
        if x_scores[-1] < lof_threshold:
            # Yes it is, add it to the dataset
            df_outlier = pd.DataFrame([outlier], columns=df.columns[:-1])
            df_outlier['class'] = label
            df = df.append(df_outlier, ignore_index=True)            
            n_outliers = n_outliers - 1
    
    # Check it managed to successfully add all the required outliers
    if n_outliers > 0:
        raise Exception("Failed to add require quantity of outliers, consider adjusting the lof_threshold, providing a bigger outlier sample, making sure the dataset is normalised between 1 and 0")
    
    return df
    

def add_outliers(df, n_outliers, random_state=1, lof_neighbors=10, lof_threshold=-2):
    
    # Loop over each label
    for label in df[df.columns[-1]].unique():

        # caluate the center (mean) of data
        center = find_center(df[df[df.columns[-1]] == label])

        # create a guassian blob at the center piont
        blob = create_blob(center, len(df.columns)-1, random_state)

        # Add true outliers
        df = add_lof_outliers(df, label, n_outliers, lof_neighbors, lof_threshold, blob)
        
    return df